
class MenuBehavior extends Sup.Behavior {
    
    awake() {
        for(var i=0;i<5;i++)
            Sup.appendScene("Menu/Shape/Prefab");
    }

    update() {
        if(Sup.Input.wasKeyJustReleased("SPACE"))
            Sup.loadScene("Game/Scene");
    }
    
}

Sup.registerBehavior(MenuBehavior);
