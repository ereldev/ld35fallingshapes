
class MenuShapeBehavior extends Sup.Behavior {
    
    public spawnDuration: number;
    
    private state: number;
    private stateSpawn = 1;
    private stateWait = 2;
    private stateDisspawn = 3;
    
    private waitDuration: number;
    private time: number;

    awake() {
        this.respawn();
    }

    update() {
        this.time++;
        
        switch(this.state) {
            case this.stateSpawn:
                var progress = this.time * 0.5 / this.spawnDuration;
                if(progress == 0) progress = 0.01;

                this.actor.setLocalScale(progress, progress, progress);
                
                if(this.time >= this.spawnDuration) {
                    this.state = this.stateWait;
                    this.time = 0;
                }
                break;
            case this.stateWait:
                if(this.time >= this.waitDuration) {
                    this.state = this.stateDisspawn;
                    this.time = 0;
                }
                break;
            case this.stateDisspawn:
                var progress = this.time * 0.5 / this.spawnDuration;
                if(progress == 0) progress = 0.01;

                this.actor.setLocalScale(0.5-progress, 0.5-progress, 0.5-progress);
                
                if(this.time >= this.spawnDuration) {
                    this.respawn();
                }
                break;
        }
    }

    private respawn() {
        var type = Sup.Math.Random.integer(0, PlayerBehavior.shapes.length-1);
        var sprite = PlayerBehavior.shapes[type];
        this.actor.spriteRenderer.setSprite(sprite);
        this.actor.setLocalScale(0.1, 0.1, 0.1);
        
        var color = new Sup.Color(Sup.Math.Random.float(0.1, 0.9), Sup.Math.Random.float(0.1, 0.9), Sup.Math.Random.float(0.1, 0.9));
        this.actor.spriteRenderer.setColor(color);
        
        var position = new Sup.Math.Vector3();
        position.x = Sup.Math.Random.float(-6, 6);
        position.y = Sup.Math.Random.float(-5, 5);
        position.z = -10;
        this.actor.setPosition(position);
        
        this.waitDuration = Sup.Math.Random.integer(120, 240);
        
        this.state = this.stateSpawn;
        this.time = 0;
    }
    
}

Sup.registerBehavior(MenuShapeBehavior);
