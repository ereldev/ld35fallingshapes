
class WorldBehavior extends Sup.Behavior {
    
    public static instance: WorldBehavior;
    
    public spaceBetweenWalls: number
    public collisionTolerance: number;

    public gameover: boolean;
    public score: number;

    private walls: Array<WallBehavior> = [];
    private nextWallZ: number;
    
    private player: PlayerBehavior;

    awake() {
        WorldBehavior.instance = this;
        
        this.gameover = false;
        this.score = 0;
        this.nextWallZ = -this.spaceBetweenWalls;
        
        this.player = Sup.getActor("Player").getBehavior(PlayerBehavior);
    }

    start() {
        this.spawnWall();
        this.spawnWall();
        
        UIBehavior.instance.setScore(this.score);
    }

    update() {
        if(this.gameover == false) {
            var wallPosition = this.walls[0].actor.getPosition();
            if(wallPosition.z == 0)
                return;

            var playerPosition = this.player.actor.getPosition();

            if(playerPosition.z <= wallPosition.z) {
                if(playerPosition.x <= wallPosition.x - this.collisionTolerance
                  || playerPosition.x >= wallPosition.x + this.collisionTolerance
                  || playerPosition.y <= wallPosition.y - this.collisionTolerance
                  || playerPosition.y >= wallPosition.y + this.collisionTolerance) {

                    this.gameOver();
                }
                else {
                    var wall = this.walls[0];

                    if(this.player.shapeIndex == wall.type) {
                        this.player.setColor(wall.getColor());
                        this.player.increaseFallSpeed();

                        this.walls.shift();
                        wall.actor.destroy();

                        this.score++;
                        UIBehavior.instance.setScore(this.score);

                        this.spawnWall();
                    }
                    else {
                        this.gameOver();
                    }
                }
            }
        }
        else {
            if(Sup.Input.wasKeyJustReleased("SPACE")) {
                Sup.loadScene("Game/Scene");
            }
            else if(Sup.Input.wasKeyJustReleased("ESCAPE")) {
                Sup.loadScene("Menu/Scene");
            }
        }
    }

    private gameOver() {
        this.gameover = true;
        
        UIBehavior.instance.gameOver(this.score);
    }

    private spawnWall() {
        var wall = Sup.appendScene("World/Wall/Prefab")[0].getBehavior(WallBehavior);
        wall.startZ = this.nextWallZ;
        this.walls.push(wall);
        
        this.nextWallZ -= this.spaceBetweenWalls;
    }
    
}

Sup.registerBehavior(WorldBehavior);
