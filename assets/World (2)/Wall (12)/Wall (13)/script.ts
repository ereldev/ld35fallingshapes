
class WallBehavior extends Sup.Behavior {
    
    private holes = [
        "World/Wall/Holes/Square", "World/Wall/Holes/Triangle", "World/Wall/Holes/Circle", "World/Wall/Holes/Hexagone"
    ];
    
    public positionVariationX: number;
    public positionVariationY: number;

    public startZ: number;
    public type: number;

    private wallRenderer: Sup.SpriteRenderer;
    private holeRenderer: Sup.SpriteRenderer;
    
    awake() {
        this.wallRenderer = this.actor.getChild("Sprite").spriteRenderer;
        this.holeRenderer = this.actor.getChild("Hole").spriteRenderer;
        
        this.type = Sup.Math.Random.integer(0, this.holes.length-1);
        var sprite = this.holes[this.type];
        this.holeRenderer.setSprite(sprite);
        
        var color = new Sup.Color(Sup.Math.Random.float(0.2, 0.9), Sup.Math.Random.float(0.2, 0.9), Sup.Math.Random.float(0.2, 0.9));
        this.wallRenderer.setColor(color);
        this.holeRenderer.setColor(color);
    }

    start() {
        var position = this.actor.getPosition();
        position.x = Sup.Math.Random.float(-this.positionVariationX, this.positionVariationX);
        position.y = Sup.Math.Random.float(-this.positionVariationY, this.positionVariationY);
        position.z = this.startZ;
        this.actor.setPosition(position);
    }

    update() {
        
    }
 
    public getColor(): Sup.Color {
        return this.wallRenderer.getColor();
    }

}

Sup.registerBehavior(WallBehavior);
