
class PlayerBehavior extends Sup.Behavior {
    
    public static shapes = [
        "Player/Shapes/Square", "Player/Shapes/Triangle", "Player/Shapes/Circle", "Player/Shapes/Hexagone"
    ];

	public morphDuration: number;
    public moveSpeed: number;
    public fallSpeed: number;
    public gravity: number;

	private renderer1: Sup.SpriteRenderer;
	private renderer2: Sup.SpriteRenderer;

	private morphing: boolean;
	public shapeIndex: number;
	private morphProgress: number;
    
    awake() {
        this.morphing = false;
		this.shapeIndex = 0;
		this.morphProgress = 0;
        
        this.renderer1 = this.actor.getChild("Sprite1").spriteRenderer;
        this.renderer2 = this.actor.getChild("Sprite2").spriteRenderer;

		this.renderer1.setSprite(PlayerBehavior.shapes[this.shapeIndex]);
        this.renderer2.setSprite(PlayerBehavior.shapes[this.shapeIndex]);
        this.renderer2.actor.setLocalScale(0.1, 0.1, 0.1);
        
        var color = new Sup.Color(Sup.Math.Random.float(0.1, 0.9), Sup.Math.Random.float(0.1, 0.9), Sup.Math.Random.float(0.1, 0.9));
        this.setColor(color);
    }

    update() {
        if(WorldBehavior.instance.gameover)
            return;
        
        this.move();
        
        this.morph();
        this.morphAnimation();
    }

    public setColor(color: Sup.Color) {
        this.renderer1.setColor(color);
        this.renderer2.setColor(color);
    }

    public increaseFallSpeed() {
        this.fallSpeed += this.gravity;
    }
    
    private move() {
        var position = this.actor.getPosition();
        
        if(Sup.Input.isKeyDown("LEFT")) {
            position.x -= this.moveSpeed;
        }
        else if(Sup.Input.isKeyDown("RIGHT")) {
            position.x += this.moveSpeed;
        }
        
        if(Sup.Input.isKeyDown("UP")) {
            position.y += this.moveSpeed;
        }
        else if(Sup.Input.isKeyDown("DOWN")) {
            position.y -= this.moveSpeed;
        }
        
        position.z -= this.fallSpeed;
        
        this.actor.setPosition(position);
    }

    private morph() {
        if(Sup.Input.wasKeyJustReleased("SPACE")) {
            this.shapeIndex++;
			if(this.shapeIndex >= PlayerBehavior.shapes.length) this.shapeIndex = 0;

			this.renderer2.setSprite(this.renderer1.getSprite());
			this.renderer1.setSprite(PlayerBehavior.shapes[this.shapeIndex]);
			this.morphing = true;
        }
    }

    private morphAnimation() {
        if(this.morphing == false)
			return;

		var progress = this.morphProgress / this.morphDuration;
		if(progress == 0) progress = 0.01;

		if(progress >= 1)
		{
			this.morphing = false;
			this.morphProgress = 0;
			return;
		}

		this.renderer1.actor.setLocalScale(progress, progress, progress);
		this.renderer2.actor.setLocalScale(1-progress, 1-progress, 1-progress);

		this.morphProgress++;
    }

}

Sup.registerBehavior(PlayerBehavior);
