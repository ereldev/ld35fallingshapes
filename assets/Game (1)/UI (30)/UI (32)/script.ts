
class UIBehavior extends Sup.Behavior {
    
    public static instance: UIBehavior;
    
    private score: Sup.TextRenderer;
    
    private gameover: Sup.Actor;
    private finalScore: Sup.TextRenderer;
    
    awake() {
        UIBehavior.instance = this;
        
        this.score = this.actor.getChild("Score").textRenderer;
        
        this.gameover = this.actor.getChild("Gameover");
        this.finalScore = this.gameover.getChild("FinalScore").textRenderer;
    }

    public setScore(score: number) {
        this.score.setText("Score: " + score);
    }

    public gameOver(score: number) {
        this.score.actor.setVisible(false);
        
        this.gameover.setVisible(true);
        this.finalScore.setText("Score: " + score);
    }
    
}

Sup.registerBehavior(UIBehavior);
