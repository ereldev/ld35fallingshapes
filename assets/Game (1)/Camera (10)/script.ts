
class CameraBehavior extends Sup.Behavior {
    
    public cameraSensibility: number;
    
    private target;
    
    awake() {
        this.target = Sup.getActor("Player");
    }

    update() {
        var center = new Sup.Math.Vector3(0, 0, this.target.getPosition().z);
		var position = center.lerp(this.target.getPosition(), this.cameraSensibility);
		position.z += 20;

		this.actor.setPosition(position);
    }
    
}

Sup.registerBehavior(CameraBehavior);
